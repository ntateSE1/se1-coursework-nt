
package my.Planner;

import java.sql.*;

public class DBAccess {
    
    public static Connection getConnection() throws SQLException {
        String jdbc = "jdbc:sqlite:planner.db";
        return (DriverManager.getConnection(jdbc));
    }
}