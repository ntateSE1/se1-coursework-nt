
package my.Planner;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author ntate
 */
public  class Exam {
    
    private String examID;
    private String moduleID;
    private String examTitle;
    private String examDate;
    
        /*public Exam(String eID,String mID,String eTitle,String eDate){
        examID= eID;
        moduleID=mID;
        examTitle = eTitle;
        examDate = eDate;

        }*/
    
        
    public String getexamID(){return examID;}
    public String getModuleID(){return moduleID;}
    public String getexamTitle(){return examTitle;}
    public String getexamDate(){return examDate;}
    
    public static ArrayList<String> getExamNames(){
        ArrayList<String> examNames = new ArrayList<String>();
       try
        {
            
            Connection con = DBAccess.getConnection();
            Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("SELECT examTitle From Exam");
            while(rs.next()){
            examNames.add(rs.getString("examTitle"));
            
            }
        
        }
        catch(SQLException e)
        {
                System.err.println(e.getMessage());
        }  
        
        return examNames;
    }
    
    
    
    
    
}

