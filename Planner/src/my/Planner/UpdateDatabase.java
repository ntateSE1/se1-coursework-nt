package my.Planner;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.util.ArrayList;
/**
 *
 * @author ntate
 */
public class UpdateDatabase {
    
    
   //Connection connection = null;
   //Statement statement = null;   
    // method to make one update to the database file
   // public static void connect() throws SQLException{
        
    //    try
     //   {
          // create a database connection
        // connection = DBAccess.getConnection();
         // statement = connection.createStatement();
        //  statement.setQueryTimeout(30);  // set timeout to 30 sec.
          
      //  }
      //  catch(SQLException e)
     //   {
     //           System.err.println(e.getMessage());
    //    }
  //  }
    
    
    
    // method to create the inital DB file. (tables ect)
    public static void createDB(){
     Connection connection = null;
   Statement statement = null; 
      try
        {
            // create a database connection
          connection = DBAccess.getConnection();
          statement = connection.createStatement();
          statement.setQueryTimeout(30);  // set timeout to 30 sec.
        statement.executeUpdate("DROP TABLE IF EXISTS coursework");
        statement.executeUpdate("DROP TABLE IF EXISTS Module");
        statement.executeUpdate("DROP TABLE IF EXISTS Task");
        statement.executeUpdate("DROP TABLE IF EXISTS Activity");
        statement.executeUpdate("DROP TABLE IF EXISTS Exam");
        //create coursework table
        statement.executeUpdate("CREATE TABLE coursework (\n" +
"	courseworkID string,\n" +
"	moduleID string,\n" +
"	courseworkTitle string,\n" +
"	courseworkDeadline datetime,\n" +
"	courseworkDescription string,\n" +
"	courseworkProgress integer\n" +
");");
        //create module table
         statement.executeUpdate("CREATE TABLE Module (\n" +
"	moduleID string,\n" +
"	moduleTitle string,\n" +
"	semester integer\n" +
");");
         
         //create task table
         statement.executeUpdate("CREATE TABLE Task (\n" +
                 "	taskID INTEGER PRIMARY KEY AUTOINCREMENT,\n" +
                 "	courseworkID string,\n" +
                 "	taskTitle string,\n" +
                 "	taskPercentage integer,\n" +
                 "	taskCompletion String\n" +
                 ");");
         
        //create activity table
         statement.executeUpdate("CREATE TABLE Activity (\n" +
"	activityID integer,\n" +
"	taskID integer,\n" +
"	activityTitle string\n" +
");"); 
         //create exam table
         statement.executeUpdate(" CREATE TABLE Exam (\n" +
"	ExamID string,\n" +
"	moduleID string,\n" +
"	examTitle string,\n" +
"	examDate datetime\n" +
");");
         
         
       }
        catch(SQLException e)
        {
                System.err.println(e.getMessage());
        } 
    }
    
    public static void newSemester(String file){
        Connection connection = null;
   Statement statement = null; 
     PreparedStatement updateCoursework; 
      PreparedStatement updateModule; 
      PreparedStatement updateExam; 
      PreparedStatement updateTask;
      try
        {
             // create a database connection
          connection = DBAccess.getConnection();
          statement = connection.createStatement();
          statement.setQueryTimeout(30);  // set timeout to 30 sec.
        
        BufferedReader buffer = null;
		
		try {
			String line;
			buffer = new BufferedReader(new FileReader(file));
			
			// Read the custon CSV file in line by line then check
                        //which table data is to be added to
			while ((line = buffer.readLine()) != null) {
				ArrayList<String> csvArray = CSVtoArrayList(line);
				if ("Module".equals(csvArray.get(0))) {
                                    updateModule = connection.prepareStatement
                                    ("INSERT INTO Module VALUES (?,?,?)") ;
                                    updateModule.setString(1, csvArray.get(1));
                                    updateModule.setString(2, csvArray.get(2));
                                    updateModule.setString(3, csvArray.get(3));
                                    updateModule.executeUpdate();
                                    
                                } else if ("coursework".equals(csvArray.get(0))){
                                updateCoursework = connection.prepareStatement
                            ("INSERT INTO coursework(courseworkID, moduleID, "
                            + "courseworkTitle, courseworkDeadline,"
                            + "courseworkDescription,courseworkProgress) "
                            + "VALUES (?,?,?,?,?,?)") ;    
                                updateCoursework.setString(1, csvArray.get(1));
                                updateCoursework.setString(2, csvArray.get(2));
                                updateCoursework.setString(3, csvArray.get(3));
                                updateCoursework.setString(4, csvArray.get(4));
                                updateCoursework.setString(5, csvArray.get(5));
                                updateCoursework.setString(6, csvArray.get(6));
                                updateCoursework.executeUpdate();
                                
                                } else if ("Exam".equals(csvArray.get(0))) {
                                updateExam = connection.prepareStatement
                                ("INSERT INTO Exam VALUES (?,?,?,?)") ;
                                updateExam.setString(1, csvArray.get(1));
                                updateExam.setString(2, csvArray.get(2));
                                updateExam.setString(3, csvArray.get(3));
                                updateExam.setString(4, csvArray.get(4));
                                updateExam.executeUpdate();
                                
                                } else if ("Task".equals(csvArray.get(0))) {
                                updateTask = connection.prepareStatement
                                ("INSERT INTO Task VALUES (?,?,?,?,?)") ;
                                updateTask.setString(1, null);
                                updateTask.setString(2, csvArray.get(2));
                                updateTask.setString(3, csvArray.get(3));
                                updateTask.setString(4, csvArray.get(4));
                                updateTask.setString(5, csvArray.get(5));
                                updateTask.executeUpdate();
                                }
                            }
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (buffer != null) buffer.close();
			} catch (IOException Exception) {
				Exception.printStackTrace();
			}
		}
            
            
            
            
            
            
            
            
         
         
         
       }
        catch(SQLException e)
        {
                System.err.println(e.getMessage());
        } 
    }
    
    // convert CSV to ArrayList 
    public static ArrayList<String> CSVtoArrayList(String csv) {
		ArrayList<String> csvArray = new ArrayList<String>();
		
		if (csv != null) {
			String[] splitData = csv.split("\\s*,\\s*");
			for (int i = 0; i < splitData.length; i++) {
				if (!(splitData[i] == null) || !(splitData[i].length() == 0)) {
					csvArray.add(splitData[i].trim());
				}
			}
		}
		
		return csvArray;
	}
    
    
   /* public static ArrayList<Exam> getExams(){
        ArrayList<Exam> exams = new ArrayList<Exam>();
        
        
        try
        {
            Connection con = DBAccess.getConnection();
            Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("SELECT * From Exams");
            while(rs.next()){
            exams.add(new Exam(rs.getString("ExamID"),rs.getString("moduleID"),rs.getString("examTitle"),rs.getString("examDate")));
            }
        
        }
        catch(SQLException e)
        {
                System.err.println(e.getMessage());
        } 
        return exams;
    }
   */
   /* public static ArrayList<Module> getModules(){
        ArrayList<Module> modules = new ArrayList<Module>();
        
        
        try
        {
            Connection con = DBAccess.getConnection();
            Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("SELECT * From Module");
            while(rs.next()){
            modules.add(new Module(rs.getString("moduleID"),rs.getString("moduleTitle"),rs.getInt("semester")));
            }
        
        }
        catch(SQLException e)
        {
                System.err.println(e.getMessage());
        } 
        return modules;
    }   */ 
   /* public static ArrayList<Coursework> getCoursework(){
        ArrayList<Coursework> courseworks = new ArrayList<Coursework>();
        
        
        try
        {
            Connection con = DBAccess.getConnection();
            Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("SELECT * From coursework");
            while(rs.next()){
            courseworks.add(new Coursework(rs.getString("courseworkID"),rs.getString("moduleID"),
                    rs.getString("courseworkTitle"),rs.getString("courseworkDeadline"),
                    rs.getString("courseworkDescription"), rs.getInt("courseworkProgress")));
            }
        
        }
        catch(SQLException e)
        {
                System.err.println(e.getMessage());
        } 
        return courseworks;
    }      */
}


