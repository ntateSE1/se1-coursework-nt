/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.Planner;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ntate
 */
public class Module {
   
   
    
    public static DefaultTableModel getTableUpdate(String md, DefaultTableModel model){
        DefaultTableModel m = model;
        try
        {
            
            Connection con = DBAccess.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT moduleID, semester FROM Module WHERE moduleTitle=\""+ md +"\"");
                       
            
                m.setRowCount(0);
        
        int columns = rs.getMetaData().getColumnCount(); 
        while(rs.next())
        {  
            Object[] row = new Object[columns];
            for (int i = 1; i <= columns; i++)
            {  
                row[i - 1] = rs.getObject(i);
            }
            (m).insertRow(rs.getRow()-1,row);
        }
        rs.close();
            st.close();
            con.close();
            
        }
        catch(SQLException e)
        {
                System.err.println(e.getMessage());
        }
        return m;
    }
    public static DefaultTableModel getCWTable(String md, DefaultTableModel model){
        DefaultTableModel m = model;
        try
        {
            
            Connection con = DBAccess.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT courseworkID,courseworkTitle,courseworkDeadline,courseworkDescription,courseworkProgress From coursework WHERE moduleID=\""+ md +"\"");
                       
            
                m.setRowCount(0);
        
        int columns = rs.getMetaData().getColumnCount(); 
        while(rs.next())
        {  
            Object[] row = new Object[columns];
            for (int i = 1; i <= columns; i++)
            {  
                row[i - 1] = rs.getObject(i);
            }
            (m).insertRow(rs.getRow()-1,row);
        }
        rs.close();
            st.close();
            con.close();
            
        }
        catch(SQLException e)
        {
                System.err.println(e.getMessage());
        }
        return m;
    }
    public static DefaultTableModel getETable(String md, DefaultTableModel model){
        DefaultTableModel m = model;
        try
        {
            
            Connection con = DBAccess.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT ExamID,examTitle,examDate From Exam WHERE moduleID=\""+ md +"\"");
                       
            
                m.setRowCount(0);
        
        int columns = rs.getMetaData().getColumnCount(); 
        while(rs.next())
        {  
            Object[] row = new Object[columns];
            for (int i = 1; i <= columns; i++)
            {  
                row[i - 1] = rs.getObject(i);
            }
            (m).insertRow(rs.getRow()-1,row);
        }
        rs.close();
            st.close();
            con.close();
            
        }
        catch(SQLException e)
        {
                System.err.println(e.getMessage());
        }
        return m;
    }
    public static String getModuleID(String md){
                       String mID = null;
        try
        {
            Connection con = DBAccess.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT moduleID From Module WHERE moduleTitle=\""+ md +"\"");
            
            mID = rs.getString("moduleID");
            
            rs.close();
            st.close();
            con.close();
        }
        catch(SQLException e)
        {
                System.err.println(e.getMessage());
        }
        
        return mID;
    }
    
    
}
