/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.Planner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author ntate
 */
public class Task {
    
    
    public static void newTask(String cID, String tN, int tP){
            Connection connection = null;
   Statement statement = null; 
     
      PreparedStatement updateTask;
      try
        {
             // create a database connection
          connection = DBAccess.getConnection();
          statement = connection.createStatement();
          statement.setQueryTimeout(30);  // set timeout to 30 sec.
    
     updateTask = connection.prepareStatement
                                ("INSERT INTO Task VALUES (?,?,?,?,?)") ;
                                updateTask.setString(1, null);
                                updateTask.setString(2, cID);
                                updateTask.setString(3, tN);
                                updateTask.setInt(4, tP);
                                updateTask.setString(5, "No");
                                updateTask.executeUpdate();
    }
        catch(SQLException e)
        {
                System.err.println(e.getMessage());
        } 
    }
    public static void setComplete(int tID, int cP, String cw){
        String cID = Coursework.getCourseworkIDFromName(cw);
        int prog = cP + Coursework.getCourseworkProgressFromID(cID);
          Connection connection = null;
   Statement statement = null; 
   
     String y = "Yes";
      PreparedStatement updateTask;
      PreparedStatement updateCw;
      try
        {
             // create a database connection
          connection = DBAccess.getConnection();
          statement = connection.createStatement();
          statement.setQueryTimeout(30);  // set timeout to 30 sec.
          
        updateTask = connection.prepareStatement
                                ("UPDATE Task SET taskCompletion= ? WHERE taskID = ?");
                                updateTask.setString(1, y);
                                updateTask.setInt(2, tID);
          updateTask.executeUpdate();
          
          
            updateCw= connection.prepareStatement
                                ("UPDATE coursework SET courseworkProgress= ? WHERE courseworkID = ?");
                                updateCw.setInt(1, prog);
                                updateCw.setString(2, cID);
          updateCw.executeUpdate();
          
        }
        catch(SQLException e)
        {
                System.err.println(e.getMessage());
        } 
    }
        
    
}
