package my.Planner;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ntate
 */
public class Coursework {
    

   
    public static DefaultTableModel getTableUpdate(String cw, DefaultTableModel model){
        DefaultTableModel m = model;
        try
        {
            
            Connection con = DBAccess.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT courseworkID,moduleID,courseworkDeadline,courseworkDescription From coursework WHERE courseworkTitle=\""+ cw +"\"");
                       
            
                m.setRowCount(0);
        
        int columns = rs.getMetaData().getColumnCount(); 
        while(rs.next())
        {  
            Object[] row = new Object[columns];
            for (int i = 1; i <= columns; i++)
            {  
                row[i - 1] = rs.getObject(i);
            }
            (m).insertRow(rs.getRow()-1,row);
        }
        rs.close();
            st.close();
            con.close();
            
        }
        catch(SQLException e)
        {
                System.err.println(e.getMessage());
        }
        return m;
    }
 
    public static String getCourseworkIDFromName(String cw){
               String mID=null;
        try
        {
            Connection con = DBAccess.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * From coursework WHERE courseworkTitle=\""+ cw +"\"");
            
            mID = rs.getString("courseworkID");
            
            rs.close();
            st.close();
            con.close();
        }
        catch(SQLException e)
        {
                System.err.println(e.getMessage());
        }
        
        return mID;
    }
    public static int getCourseworkProgress(String cw){
                       int cProg = 0;
        try
        {
            Connection con = DBAccess.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * From coursework WHERE courseworkTitle=\""+ cw +"\"");
            
            cProg = rs.getInt("courseworkProgress");
            
            rs.close();
            st.close();
            con.close();
        }
        catch(SQLException e)
        {
                System.err.println(e.getMessage());
        }
        
        return cProg;
    }
        public static int getCourseworkProgressFromID(String cID){
                       int cProg = 0;
        try
        {
            Connection con = DBAccess.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * From coursework WHERE courseworkID=\""+ cID +"\"");
            
            cProg = rs.getInt("courseworkProgress");
            
            rs.close();
            st.close();
            con.close();
        }
        catch(SQLException e)
        {
                System.err.println(e.getMessage());
        }
        
        return cProg;
    }
    public static String getCourseworkProgressString(String cw){
                       String cProg = null;
        try
        {
            Connection con = DBAccess.getConnection();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * From coursework WHERE courseworkTitle=\""+ cw +"\"");
            
            cProg = rs.getString("courseworkProgress".toString());
            
            rs.close();
            st.close();
            con.close();
        }
        catch(SQLException e)
        {
                System.err.println(e.getMessage());
        }
        
        return cProg;
    }
    
    
}
